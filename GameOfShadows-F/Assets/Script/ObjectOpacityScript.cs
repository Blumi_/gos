﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectOpacityScript : MonoBehaviour
{
    [SerializeField] private float opacity = 0f;

    void Update()
    {
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, opacity);

        if (opacity > 1)
            opacity = 1;
    }

    public void OnParticleCollision (GameObject other)
    {
        opacity += .1f;
    }
}
