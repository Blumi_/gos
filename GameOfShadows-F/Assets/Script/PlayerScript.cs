﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    //Vars
    [Header("Parameters")]
    [SerializeField] private float speed;

    #region Jump Vars
    private float jumpPressedRemember = 0;
    [SerializeField] private float jumpPressedRememberTime = 0.2f;

    private float groundedRemember = 0;
    [SerializeField] private float groundedRememberTime = 0.25f;

    [SerializeField] float jumpForce = 5;

    [SerializeField] private float fallMultiplier = 2.5f;
    [SerializeField] private float lowJumpMultiplier = 2f;
    #endregion

    //Debug Vars
    [Header("Debug")]
    [SerializeField] private bool isMoving;
    [SerializeField] private bool death;
    //Jump
    [SerializeField] private bool grounded;
    public Transform groundCheck;
    public float groundRadius = .08f;
    [SerializeField] LayerMask obstacle;

    private Rigidbody2D rb;
    public ParticleSystem particle;
    private GameObject deathPart;

    void Start()
    {
        groundCheck = GameObject.FindGameObjectWithTag("GroundCheck").transform;
        rb = GetComponent<Rigidbody2D>();
        particle = GameObject.FindGameObjectWithTag("MoveParticle").GetComponent<ParticleSystem>();
        
    }

    void FixedUpdate()
    {
        if(groundCheck != null)    
            grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, obstacle);

        #region Jump
        groundedRemember -= Time.deltaTime;

        if (grounded)
        {
            groundedRemember = groundedRememberTime;
        }

        jumpPressedRemember -= Time.deltaTime;

        if (Input.GetButtonDown("Jump"))
        {
            jumpPressedRemember = jumpPressedRememberTime;
        }

        if ((jumpPressedRemember > 0) && (groundedRemember > 0))
        {
            jumpPressedRemember = 0;
            groundedRemember = 0;
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }

        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }

        #endregion

        float move = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(move * speed, rb.velocity.y);

        if(move != 0 || grounded == false)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

        ParticleSystem();
    }

    void ParticleSystem()
    {
        ParticleSystem.EmissionModule emission = particle.emission;

        if (isMoving == true)
        {
            emission.enabled = true;
        }
        else
        {
            emission.enabled = false;
        }
    }
}
